var uic = {

	//参考：http://www.htmleaf.com/ziliaoku/qianduanjiaocheng/20141225979.html

	//ICON-LIKE
	"smiley": "☺",
	"steam": "♨",
	"ball_8": "➑",
	"star": "★",
	"star_o": "☆",
	"bars": "♡",
	"heart": "❤",
	"plane": "✈",
	"scissors": "✂",
	"scissors_o": "✄",
	"crown": "♕",
	"cross": "✝",
	"adjust": "◑",
	"note_8": "♪",
	"note_16": "♫",
	"asterisk_4": "✣",
	"ball_star": "✪",
	"star_shadow": "✰",
	"star_4p_o": "✧",
	"star_4p": "✦",
	"check_square": "☑",
	"v": "✔",
	"x": "✘",
	"pencil": "✎",
	"hand_writing": "✍",
	"female": "♀",
	"male": "♂",
	"telephone": "☎",
	"telephone_o": "",
	"envelope": "✉",
	"location_telephone": "✆",

	//ARROWS
	"arrow_left": "←",
	"arrow_right": "→",
	"arrow_up": "↑",
	"arrow_down": "↓",
	"arrows_h": "↔",
	"arrows_v": "↕",
	"arrow_lar, exchange": "⇄",
	"arrow_uad": "⇅",
	"arrow_d2l": "↲",
	"arrow_d2r": "↳",
	"arrow_u2l": "↰",
	"arrow_u2r": "↱",
	"arrow_left_end": "⇤",
	"arrow_right_end": "⇥",
	"arrow_left_semicircle": "↶",
	"arrow_right_semicircle": "↷",
	"rotate_left, undo": "↺",
	"rotate_right, repeat": "↻",
	"arrow_right_wide": "➔",
	"arrow_flash": "↯",
	"arrow_rd2lu": "↖",
	"arrow_lu2rd_heavy": "➘",
	"arrow_right_heavy": "➙",
	"arrow_ld2ru_heavy": "➚",
	"arrow_right_dashed": "➟",
	"arrow_left_dotted": "⇠",
	"arrowhead_right": "➤",
	"arrow_left_o": "⇦",
	"arrow_right_o": "⇨",
	"angle_double_left": "«",
	"angle_doubl_right": "»",
	"caret_left": "►",
	"caret_right": "◀",
	"caret_up": "▲",
	"caret_down": "▼",
	"caret_left_o": "▷",
	"caret_right_o": "◁",
	"caret_up_o": "△",
	"caret_down_o": "▽",
	"arrow_bow": "➴",
};

var css = "@charset \"utf-8\";";
var tab = "|Name|Preview|" + "\r\n" + "|:-----:|:-----:|" + "\r\n";

for (var name in uic) {
	var nameList = name.split(", ");
	var fullName = "";

	for (var i = 0; i < nameList.length; i++) {
		fullName += ".uic-" + nameList[i] + ":before,.uic-" + nameList[i] + "-after:after,";
	};

	css += fullName.slice(0, -1) + "{content:\"" + uic[name] + "\";}";
	tab += "|" + name + "|" + uic[name] + "|" + "\r\n";
};

var fs = require('fs');

fs.writeFile("unico.css", css);
fs.writeFile("table.md", tab);